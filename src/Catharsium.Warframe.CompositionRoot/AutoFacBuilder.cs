﻿using Autofac;
using Catharsium.Warframe.Data.Alerts;
using Catharsium.Warframe.Entities.Alerts;

namespace Catharsium.Warframe.CompositionRoot
{
    public class AutoFacBuilder
    {
        public IContainer Build()
        {
            var result = new ContainerBuilder();

            result.RegisterType<AlertService>().As<IAlertService>();
            result.RegisterType<AlertProvider>().As<IAlertProvider>();

            return result.Build();
        }
    }
}