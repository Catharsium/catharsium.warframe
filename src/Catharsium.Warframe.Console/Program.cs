﻿using Autofac;
using Catharsium.Warframe.CompositionRoot;
using Catharsium.Warframe.Entities.Alerts;

namespace Catharsium.Warframe.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var container = new AutoFacBuilder().Build();
            using(var scope = container.BeginLifetimeScope())
            {
                var provider = scope.Resolve<IAlertProvider>();
                foreach(var alert in provider.GetList())
                {
                    System.Console.WriteLine(alert);
                }
                System.Console.ReadLine();
            }
        }
    }
}