﻿using System.Collections.Generic;

namespace Catharsium.Warframe.Entities.Alerts
{
    public interface IAlertProvider
    {
        IEnumerable<IAlert> GetList();
    }
}