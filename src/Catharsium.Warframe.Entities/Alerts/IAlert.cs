﻿using Catharsium.Warframe.Entities.Factions;
using System;

namespace Catharsium.Warframe.Entities.Alerts
{
    public interface IAlert
    {
        Guid ID { get; }
        string Title { get; }
        string Author { get; }
        string Description { get; }
        DateTime PublishingDate { get; }
        DateTime ExperationDate { get; }
        Faction Faction { get; }
    }
}