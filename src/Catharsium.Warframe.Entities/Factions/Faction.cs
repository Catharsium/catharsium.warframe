﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catharsium.Warframe.Entities.Factions
{
    public enum Faction
    {
        Grineer,
        Infestation
    }
}
