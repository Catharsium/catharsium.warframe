﻿using System.Collections.Generic;
using Catharsium.Warframe.Entities.Alerts;

namespace Catharsium.Warframe.Data.Alerts
{
    public class AlertProvider : IAlertProvider
    {
        #region Properties

        IAlertService AlertService { get; set; }

        #endregion

        #region Construction

        public AlertProvider(IAlertService alertService)
        {
            AlertService = alertService;
        }

        #endregion

        #region IAlertProvider

        public IEnumerable<IAlert> GetList()
        {
            return new Alert[]
            {
                new Alert { Author = "Author", Description = "Description" }
            };
        }

        #endregion
    }
}