﻿using Catharsium.Warframe.Entities.Alerts;
using System;
using Catharsium.Warframe.Entities.Factions;

namespace Catharsium.Warframe.Data.Alerts
{
    public class Alert : IAlert
    {
        #region Properties

        public string Author { get; set; }

        public string Description { get; set; }

        public DateTime ExperationDate { get; set; }

        public Faction Faction { get; set; }

        public Guid ID { get; set; }

        public DateTime PublishingDate { get; set; }

        public string Title { get; set; }

        #endregion

        #region Transformation

        public override string ToString()
        {
            return string.Format("Author: {0}, Description: {1}", Author, Description);
        }

        #endregion
    }
}